package com.demoapp.crudappjava.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Entity
@Table(name = "books")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    Integer id ;

    @Column(length = 255, unique = true, nullable = false)
    String name;

    @Column(length = 255)
    String Author;

    @Column(length = 255)
    String ReleasedYear;


}
