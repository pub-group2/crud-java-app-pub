package com.demoapp.crudappjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudAppJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudAppJavaApplication.class, args);
	}

}
