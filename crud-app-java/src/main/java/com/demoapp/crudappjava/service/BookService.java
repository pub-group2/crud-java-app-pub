package com.demoapp.crudappjava.service;

import com.demoapp.crudappjava.model.Book;
import com.demoapp.crudappjava.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Random;

@Service
public class BookService {
    @Autowired
    BookRepository bookRepository;
    private void createBook(){
        Book book = new Book();
        book.setName("book-"+String.valueOf(System.currentTimeMillis()));
        book.setAuthor("Auther-"+String.valueOf(System.currentTimeMillis()));
        book.setReleasedYear("2010");
        bookRepository.save(book);
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    public List<Book> getAllBooks() {
        createBook();
        return bookRepository.findAll();
    }

    public String updateBook(Book book) {
        String response = "Book with ID : " + book.getId();
        Book oldBook = new Book();

        if(bookRepository.existsById(book.getId()))
        {
            oldBook = bookRepository.getReferenceById(book.getId());
            oldBook.setId(book.getId());
            oldBook.setName(book.getName());
            oldBook.setAuthor(book.getAuthor());
            oldBook.setReleasedYear(book.getReleasedYear());

            response = bookRepository.save(book).toString();
        }else {
            response += " Does not exists.";
        }
        return response.toString();
    }

    public String deleteBook(Integer id) {
        String response = "Book with ID " + id;
        if (bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
            response += " Deleted Successfully.";
        } else {
            response += " Does not exists.";
        }
        return response.toString();
    }

    public String deleteAllBook() {
        bookRepository.deleteAll();
        return "All Books Deleted Successfully !!";
    }


    public String getAppHealth() {
        String status = null;
        try {
            if(bookRepository.count() >= 0){
                status = "Application is Healthy !!";
            }
        }catch (Exception e){
            status = "Application is Not Healthy !!";
        }
        return status;
    }


    
}
