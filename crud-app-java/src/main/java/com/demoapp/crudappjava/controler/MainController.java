package com.demoapp.crudappjava.controler;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

   @GetMapping(path = "/index")
    public String index(Model model) {
        model.addAttribute("name","wimarsha");
        return "index";
    }
}
