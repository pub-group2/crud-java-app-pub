package com.demoapp.crudappjava.controler;

import com.demoapp.crudappjava.model.Book;
import com.demoapp.crudappjava.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping(path = "/book")
public class BookControler {

    @Autowired
    BookService bookService;

    @GetMapping()
    public String getName() {
        return "wimarsha";
    }

    @GetMapping(path = "/all")
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @PostMapping
    public Book createBook(@RequestBody Book book){
        return bookService.createBook(book);
    }

    @PutMapping(path = "/update")
    public String updateBook(@RequestBody Book book){
        if (null == book || 0 == book.getId() || null == book.getName() ) {
            System.out.println("Required Fields Not Found");
        }
        return bookService.updateBook(book);
    }

    @DeleteMapping(path = "/delete/{id}")
    public String deleteBook(@PathVariable("id") Integer id) {
        return bookService.deleteBook(id);
    }

    @DeleteMapping(path = "/deleteAll")
    public String deleteAllBook() {
        return bookService.deleteAllBook();
    }

    @GetMapping(path = "/health")
    public String getAppHealth() {
        return bookService.getAppHealth();
    }

}
